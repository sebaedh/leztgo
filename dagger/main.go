// A generated module for Leztgo functions
//
// This module has been generated via dagger init and serves as a reference to
// basic module structure as you get started with Dagger.
//
// Two functions have been pre-created. You can modify, delete, or add to them,
// as needed. They demonstrate usage of arguments and return types using simple
// echo and grep commands. The functions can be called from the dagger CLI or
// from one of the SDKs.
//
// The first line in this comment block is a short description line and the
// rest is a long description with more detail on the module's purpose or usage,
// if appropriate. All modules should have a short description.

package main

import (
	"context"
	"dagger/leztgo/internal/dagger"
)

type Leztgo struct{}

// Returns a container that echoes whatever string argument is provided
func (m *Leztgo) ContainerEcho(stringArg string) *Container {
	return dag.Container().From("alpine:latest").WithExec([]string{"echo", stringArg})
}

// Returns lines that match a pattern in the files of the provided Directory
func (m *Leztgo) GrepDir(ctx context.Context, directoryArg *Directory, pattern string) (string, error) {
	return dag.Container().
		From("alpine:latest").
		WithMountedDirectory("/mnt", directoryArg).
		WithWorkdir("/mnt").
		WithExec([]string{"grep", "-R", pattern, "."}).
		Stdout(ctx)
}

func (m *Leztgo) Build(
	ctx context.Context,
	src *Directory,
	dockerfile *File,
) (string, error) {
	workspace := dag.Container().
		WithDirectory("/src", src).
		WithWorkdir("/src").
		WithFile("/src/custom.Dockerfile", dockerfile).
		Directory("/src")

	// build using Dockerfile and publish to registry
	ref, err := dag.Container().
		Build(workspace, dagger.ContainerBuildOpts{
			Dockerfile: "custom.Dockerfile",
		}).
		Publish(ctx, "ttl.sh/hello-dagger")

	if err != nil {
		return "", nil
	}

	return ref, nil
}
