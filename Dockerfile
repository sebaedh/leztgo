FROM alpine:latest

ARG OPENBAO_VERSION=v2.0.0-alpha20240329

RUN wget -qO- https://github.com/openbao/openbao/releases/download/${OPENBAO_VERSION}/${OPENBAO_VERSION}_linux_amd64.zip \
  unzip -d /usr/local/bin
